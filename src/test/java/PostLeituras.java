

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class PostLeituras {

  static Logger logger = LoggerFactory.getLogger(PostLeituras.class);

  public static void main(String[] args) throws Throwable {
    
    int interval = 15;
    LocalDateTime time = LocalDateTime.now();
    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss");
    
    int numLeituras = 1;
    
    double tensao1 = 118.16170830279589;
    double tensao2 = 220.16170830279589;
    double tensao3 = 330.16170830279589;
    
    try (CloseableHttpClient client = HttpClients.createDefault()) {
//      for (int m = 1; m <= 3; m++) {
        time = LocalDateTime.now();
        for (int i = 0; i < numLeituras; i++) {

          Map<String, String> map = new HashMap<>();
          map.put("mci", "0107898922479454210090679");
          map.put("phaseA", Double.toString(tensao1));
          map.put("phaseB", Double.toString(tensao2));
          map.put("phaseC", Double.toString(tensao3));
          map.put("dateTime", time.format(df));

          time = time.plusMinutes(interval);
          tensao1 = 100 + Math.random()*100;
          tensao2 = 100 + Math.random()*100;
          tensao3 = 100 + Math.random()*100;

          String json = new Gson().toJson(map);
          StringEntity entity = new StringEntity(json.toString());
          HttpPost httpPost = new HttpPost(
              "http://localhost:8180/monitor-ramal/resources/readings/salvar");
          httpPost.setEntity(entity);
          httpPost.setHeader("Content-type", "application/json");

          try (CloseableHttpResponse response = client.execute(httpPost)) {
            logger.debug("Response: {}", response.getStatusLine());
          } catch (Exception e) {
            logger.error("Posting", e);
          }
//        }
      }
    } catch (Exception e) {
      logger.error("Posting", e);
    }
  }
}
