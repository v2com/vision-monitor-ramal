import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.v2com.vision.monitor.ramal.entity.MeterDAO;
import com.v2com.vision.monitor.ramal.entity.registry.Meter;
import com.v2com.vision.monitor.ramal.entity.registry.XYZ;

public class CriaMeter {

  public static void main(String[] args) throws Exception {
   
    List<Meter> meters = new ArrayList<Meter>();
    for (int i = 1; i <= 3; i++) {
      Meter meter = new Meter();
      meter.setNome("Submeter " + i);
      meter.setSerialNumber("0107898922479461210000001");
      meter.setPosition(new XYZ(-46.70955764892135,-23.55077320839725));
      meters.add(meter);
    }

    MeterDAO submeterDAO = new MeterDAO();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    Files.write(Paths.get("/tmp/meters.json"), gson.toJson(meters).getBytes());
    
    meters = submeterDAO.loadAll();
    System.out.println(meters);
  }

}
