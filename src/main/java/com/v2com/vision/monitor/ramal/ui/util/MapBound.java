package com.v2com.vision.monitor.ramal.ui.util;

import com.vaadin.tapio.googlemaps.client.LatLon;

public class MapBound {
  
  private LatLon boundsne;
  private LatLon boundssw;

  public void extendBounds(LatLon coord) {
    if (boundsne == null) {
      boundsne = coord;
    }
    if (boundssw == null) {
      boundssw = coord;
    }

    double maxLat = Math.max(coord.getLat(), boundsne.getLat());
    double maxLon = Math.max(coord.getLon(), boundsne.getLon());
    boundsne = new LatLon(maxLat, maxLon);

    double minLat = Math.min(coord.getLat(), boundssw.getLat());
    double minLon = Math.min(coord.getLon(), boundssw.getLon());
    boundssw = new LatLon(minLat, minLon);
  }

  public LatLon getBoundsne() {
    return boundsne;
  }

  public LatLon getBoundssw() {
    return boundssw;
  }

}
