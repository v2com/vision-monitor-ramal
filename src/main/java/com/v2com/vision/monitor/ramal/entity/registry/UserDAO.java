package com.v2com.vision.monitor.ramal.entity.registry;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.entity.LocalDAO;

public class UserDAO extends LocalDAO<User> {

  private Logger logger = LoggerFactory.getLogger(getClass());
  
  @Override
  protected Path getPath() {
    if (System.getProperty("jboss.server.config.dir") != null) {
      return Paths.get(System.getProperty("jboss.server.config.dir"), "users.json");
    } else {
      return Paths.get("users.json");
    }
  }
  
  public User findByLogin(String login) {
    for (User user : loadAll()) {
      if (user.getLogin().equals(login))
        return user;
    }
    return null;
  }
  
  @Produces
  @Named("users")
  public List<User> loadAll() {
    logger.info("Carregando users...");
    return super.loadAll();
  }

}
