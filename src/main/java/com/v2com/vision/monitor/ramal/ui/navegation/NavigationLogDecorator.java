package com.v2com.vision.monitor.ramal.ui.navegation;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.ui.user.UserInfo;

@Decorator
public class NavigationLogDecorator implements NavigationService {

  private static final long serialVersionUID = -998825092520629374L;

  @Inject
  @Delegate
  @Any
  private NavigationService delegate;

  @Inject
  private UserInfo userInfo;

  @Override
  public void onNavigationEvent(NavigationEvent event) {
    LoggerFactory.getLogger(getClass()).info(
        userInfo.getUser() + " navigated to " + event.getNavigateTo());
    delegate.onNavigationEvent(event);
  }

}
