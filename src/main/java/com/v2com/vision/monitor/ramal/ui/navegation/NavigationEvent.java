package com.v2com.vision.monitor.ramal.ui.navegation;

public class NavigationEvent {
  private final String navigateTo;

  public NavigationEvent(String navigateTo) {
    this.navigateTo = navigateTo;
  }

  public String getNavigateTo() {
    return navigateTo;
  }

}
