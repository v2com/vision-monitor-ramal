package com.v2com.vision.monitor.ramal.ui.app;

import com.v2com.vision.monitor.ramal.ui.navegation.NavigationEvent;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Viewport;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import javax.inject.Inject;

@Title("V2COM Vision")
@Theme("mytheme")
@PreserveOnRefresh
@Push
@CDIUI("ui")
@Viewport("width=device-width,initial-scale=1.0,user-scalable=no")
public class VisionFleetUi extends UI {

  private static final long serialVersionUID = -4969921023185774428L;
  
  @Inject
  private javax.enterprise.event.Event<NavigationEvent> navigationEvent;

  @Override
  protected void init(VaadinRequest vaadinRequest) {
    navigationEvent.fire(new NavigationEvent("login"));
  }
}
