package com.v2com.vision.monitor.ramal.entity;


import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;

import com.v2com.vision.monitor.ramal.entity.registry.Reading;

@Stateless
public class ReadingDAO extends BaseDAO<Reading> {

  @SuppressWarnings("unchecked")
  public List<Reading> getReadings(String meter, ZonedDateTime dataInicial, ZonedDateTime dataFinal) {
    StringBuilder builder = new StringBuilder("select r from Reading r where r.meter like :meter")
        .append(" and r.datetime between :dataInicial and :dataFinal")
        .append(" order by r.datetime desc");
    
    List<Reading> list = em.createQuery(builder.toString())
        .setParameter("meter", "%"+ meter + "%")
        .setParameter("dataInicial", dataInicial)
        .setParameter("dataFinal", dataFinal)
        .getResultList();
    
    return list != null ? list : Collections.emptyList();
  }
  
  @SuppressWarnings("unchecked")
  public Reading getLastReading(String meter, ZonedDateTime dataFinal) {
    StringBuilder builder = new StringBuilder("select r from Reading r where r.meter = :meter")
        .append(" and r.datetime < :dataFinal")
        .append(" order by r.datetime desc");
    
    List<Reading> list = em.createQuery(builder.toString())
        .setParameter("meter", meter)
        .setParameter("dataFinal", dataFinal)
        .setMaxResults(1)
        .getResultList();
    
    return list != null && !list.isEmpty() ? list.get(0) : null;
  }

}
