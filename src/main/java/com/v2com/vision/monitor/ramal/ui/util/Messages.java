package com.v2com.vision.monitor.ramal.ui.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.ui.user.UserInfo;
import com.vaadin.cdi.UIScoped;
import com.vaadin.server.VaadinSession;

@UIScoped
public class Messages {

  public static final Locale ENGLISH = Locale.ENGLISH;
  public static final Locale PORTUGUESE = new Locale("pt", "BR");
 // public static final Locale SPAIN = new Locale("es", "ES");
  //public static final Locale USA = new Locale("en", "US");

  @Inject
  private UserInfo userInfo;

  private Map<String, String> messages = new HashMap<>();

  @PostConstruct
  public void loadResourceBundle() {
    loadResourceBundle(userInfo.getLocale());
  }

  /**
   * Loads the messages for a specific Locale.
   * 
   * @param locale to load, not null
   */
  private void loadResourceBundle(Locale locale) {
    LoggerFactory.getLogger(getClass()).info("Loading Resource Bundle for locale {}", locale);
    VaadinSession.getCurrent().setLocale(locale);
    ResourceBundle labels = ResourceBundle.getBundle("messages", locale);

    messages.clear();
    Enumeration<String> keys = labels.getKeys();
    while (keys.hasMoreElements()) {
      String key = keys.nextElement();
      messages.put(key, labels.getString(key));
    }
  }

  /**
   * Returns a message in the current user's locale.
   * 
   * @param reference the Class where the message is being loaded into
   * @param key the key inside the Class
   * @return the message
   */
  public String getMessage(Class<?> reference, String key) {
    Class<?> refClass = reference;
    while (!Object.class.equals(refClass)) {
      String refKey = refClass.getSimpleName() + "." + key;
      if (messages.containsKey(refKey)) {
        return messages.get(refKey);
      } else {
        refClass = refClass.getSuperclass();
      }
    }

    LoggerFactory.getLogger(getClass()).warn("Missing i18n key: {}.{}",
        reference.getSimpleName(), key);
    return "!" + reference.getSimpleName() + "." + key + "!";
  }

  /**
   * Loads the message using the specified key.
   * 
   * @param key the key to be loaded
   * @return the message corresponding to the key
   */
  public String getMessage(String key) {
    if (messages.containsKey(key)) {
      return messages.get(key);
    }

    return "!" + key + "!";
  }

  /**
   * Loads the message and apply the parameters as String.format(message, params)
   * 
   * @param reference the Class where the message is being loaded into
   * @param key the key inside the Class
   * @param params the parameters to feed in String.format()
   * @return the message returned by String.format(message, params)
   */
  public String getMessage(Class<?> reference, String key, Object... params) {
    return String.format(getMessage(reference, key), params);
  }
  
}
