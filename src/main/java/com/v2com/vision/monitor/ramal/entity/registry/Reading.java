package com.v2com.vision.monitor.ramal.entity.registry;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Reading {
  
	 @Id
	  @GeneratedValue
	  private long id;
	  
	  @Column(length = 100, nullable = false)
	  private String meter;
	  
	  @Column(nullable = true)
	  private Float voltagePh1;
	  
	  @Column(nullable = true)
	  private Float voltagePh2;
	  
	  @Column(nullable = true)
	  private Float voltagePh3;
	  
	  @Column(nullable = false)
	  private ZonedDateTime datetime;
  
  public long getId() {
    return id;
  }

  protected void setId(long id) {
    this.id = id;
  }
  
  public String getMeter() {
    return meter;
  }
  
  public void setMeter(String meter) {
    this.meter = meter;
  }


  public Float getVoltagePh1() {
    return voltagePh1;
  }
  
  public void setVoltagePh1(Float voltagePh1) {
    this.voltagePh1 = voltagePh1;
  }
 

  public Float getVoltagePh2() {
    return voltagePh2;
  }

  public void setVoltagePh2(Float voltagePh2) {
    this.voltagePh2 = voltagePh2;
  }

  public Float getVoltagePh3() {
    return voltagePh3;
  }

  public void setVoltagePh3(Float voltagePh3) {
    this.voltagePh3 = voltagePh3;
  }
  

  public ZonedDateTime getDatetime() {
    return datetime;
  }

  public void setDatetime(ZonedDateTime datetime) {
    this.datetime = datetime;
  }

  @Override
  public String toString() {
	    StringBuilder builder = new StringBuilder();
	    builder.append("Reading [id=").append(id)
	        .append(", voltagePh1=").append(voltagePh1)
	        .append(", voltagePh2=").append(voltagePh2)
	        .append(", voltagePh3=").append(voltagePh3)
	        .append(", datetime=").append(datetime)
	        .append("]");
	    return builder.toString();
  }

}
