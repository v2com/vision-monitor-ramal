package com.v2com.vision.monitor.ramal.entity;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.LoggerFactory;

@Stateless
public abstract class BaseDAO<T> {

  @PersistenceContext
  protected EntityManager em;
  
  @Transactional
  public boolean save(T entity) {
    try {
      LoggerFactory.getLogger(getClass()).debug("Persisting: {}", entity.getClass());
      em.persist(entity);
      return true;
    } catch (Exception e) {
      LoggerFactory.getLogger(getClass()).error("Persisting resource", e);
      return false;
    }
  }

}
