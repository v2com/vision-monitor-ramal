package com.v2com.vision.monitor.ramal.ui.login;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.v2com.vision.monitor.ramal.ui.navegation.NavigationEvent;
import com.v2com.vision.monitor.ramal.ui.user.LocalAccessControl;
import com.v2com.vision.monitor.ramal.ui.util.Messages;
import com.v2com.vision.monitor.ramal.ui.view.DashboardView;
import com.vaadin.cdi.CDIView;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@CDIView(LoginView.VIEWNAME)
public class LoginView extends VerticalLayout implements View {
  private static final long serialVersionUID = 1L;

  public static final String VIEWNAME = "login";

  private Panel panel;
  private VerticalLayout content;
  private HorizontalLayout titleBar;
  private FormLayout fields;
  private Label title;
  private TextField loginField;
  private PasswordField passwordField;
  private Button loginButton;

  @Inject
  private Messages messages;
  
  @Inject
  private LocalAccessControl accessControl;

  @Inject
  private javax.enterprise.event.Event<NavigationEvent> navigationEvent;
  
  /**
   * First page of application.
   */

  @PostConstruct
  public void init() {
    this.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
    this.setSizeFull();
    Responsive.makeResponsive(this);

    Image logo = new Image(null,
        new StreamResource(
            () -> Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("images/logo.png"), //$NON-NLS-1$
            "logo.png"));
    logo.setWidth("50px");

    panel = new Panel();
    panel.setSizeUndefined();
    panel.setStyleName(ValoTheme.PANEL_BORDERLESS);
    this.addComponent(panel);

    content = new VerticalLayout();
    content.setMargin(false);
    content.setSpacing(false);
    content.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

    titleBar = new HorizontalLayout();
    titleBar.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
    titleBar.setSpacing(true);
    titleBar.addComponent(logo);
    title = new Label(getMessage("Title"));
    title.addStyleName(ValoTheme.LABEL_H2);
    title.addStyleName(ValoTheme.LABEL_BOLD);
    title.addStyleName(ValoTheme.LABEL_COLORED);
    titleBar.addComponent(title);
    content.addComponent(titleBar);

    fields = new FormLayout();
    fields.setMargin(false);
    fields.setSizeUndefined();

    loginField = new TextField(getMessage("User"));
    fields.addComponent(loginField);
    passwordField = new PasswordField(getMessage("Password"));
    fields.addComponent(passwordField);
    loginButton = new Button(getMessage("Login"));
    loginButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
    loginButton.setClickShortcut(KeyCode.ENTER);
    loginButton.addClickListener((unused) -> {

      if (accessControl.doLogin(loginField.getValue(), passwordField.getValue())) {
        Notification.show(getMessage("LoginSucceeded", loginField.getValue()));
        navigationEvent.fire(new NavigationEvent(DashboardView.VIEWNAME));
      } else {
        Notification.show(getMessage("LoginFailed", loginField.getValue()), Notification.Type.WARNING_MESSAGE);
      }
    });
    fields.addComponent(loginButton);

    content.addComponent(fields);
    panel.setContent(content);

    loginField.focus();
  }
  
  private String getMessage(String key) {
    return messages.getMessage(this.getClass(), key);
  }

  private String getMessage(String key, Object... params) {
    return messages.getMessage(this.getClass(), key, params);
  }

}
