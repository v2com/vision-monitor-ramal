package com.v2com.vision.monitor.ramal.ui.view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.entity.AlarmDAO;
import com.v2com.vision.monitor.ramal.entity.registry.Alarm;
import com.v2com.vision.monitor.ramal.entity.registry.Meter;
import com.v2com.vision.monitor.ramal.rest.CommandUndervoltagesRest;
import com.v2com.vision.monitor.ramal.ui.app.BaseView;
import com.v2com.vision.monitor.ramal.ui.user.UserInfo;
import com.vaadin.cdi.CDIView;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.renderers.LocalDateTimeRenderer;

import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings({"deprecation","serial"})
@CDIView(AlarmsView.VIEWNAME)
public class AlarmsView extends BaseView {

  public static final String VIEWNAME = "AlarmsView";
  
  private static final String DATETIME_PATTERN = "dd/MM/yyyy HH:mm";

  @Inject
  @Named("meters")
  List<Meter> meters;
  
  @Inject
  private AlarmDAO alarmDAO;
  
  @Inject
  private UserInfo userInfo;
  
  private DateTimeField endDate;
  private DateTimeField startDate;
  private List<Alarm> lastReadings;
  private ComboBox<Meter> meter;
  private Grid<Alarm> grid = new Grid<>(Alarm.class);
  private DecimalFormat df = new DecimalFormat("#.##");
  private Logger logger = LoggerFactory.getLogger(getClass());
  
  @Override
  protected Component createViewComponent() {
    Panel panel = new Panel(getMessage("Title"));
    panel.setSizeFull();
    
    meter = new ComboBox<Meter>(getMessage("Module"));
    meter.setEmptySelectionAllowed(true);
    meter.setEmptySelectionCaption(getMessage("All"));
    meter.setItems(meters);
    meter.setTextInputAllowed(false);
    meter.setPopupWidth(null);
    meter.setWidth("280px");
    meter.setItemCaptionGenerator((ItemCaptionGenerator<Meter>) submeter -> {
      return submeter.getSerialNumber();
    });
    meter.focus();
    if (userInfo.getSelectedMeter() != null) {
      meter.setSelectedItem(userInfo.getSelectedMeter());
    }
    
    df.setMaximumFractionDigits(2);
    df.setMinimumFractionDigits(2);
    
    CssLayout alarmLayout = new CssLayout(meter) {
      @Override
      protected String getCss(Component c) {
        return "display: inline-block; margin-left: 8px; margin-right: 8px;";
      }
    };
    alarmLayout.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
    
   startDate = new DateTimeField(getMessage("StartDate"), LocalDateTime.now().minusDays(3));
    endDate = new DateTimeField(getMessage("EndDate"), LocalDateTime.now());

    startDate.setPlaceholder(getMessage("StartDate"));
    endDate.setPlaceholder(getMessage("EndDate"));

    startDate.setDateFormat(DATETIME_PATTERN);
    endDate.setDateFormat(DATETIME_PATTERN);

    CssLayout dateLayout = new CssLayout(startDate, endDate) {
      @Override
      protected String getCss(Component c) {
        return "display: inline-block; margin-left: 8px; margin-right: 8px;";
      }
    };
    dateLayout.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
    
    VerticalLayout fieldsLayout = new VerticalLayout(alarmLayout, dateLayout);
    fieldsLayout.setComponentAlignment(alarmLayout, Alignment.MIDDLE_CENTER);
    fieldsLayout.setComponentAlignment(dateLayout, Alignment.MIDDLE_CENTER);

    Button searchButton = new Button(FontAwesome.SEARCH);
    searchButton.addClickListener(clickListener -> updateGrid());

    Button excelButton = new Button(FontAwesome.FILE_EXCEL_O);
    excelButton.addClickListener(e -> exportGridExcel());
    
    Button commandAlarms= new Button(FontAwesome.SEND);
    commandAlarms.addClickListener(e -> sendCommandAlarms());    
    

//    excelButton.addClickListener(e -> export(ExcelExport.class, ".xls"));

//    Button csvButton = new Button(FontAwesome.FILE_CODE_O);
//    csvButton.addClickListener(e -> export(CsvExport.class, ".csv"));
    
    HorizontalLayout buttonsLayout = new HorizontalLayout(searchButton, excelButton, commandAlarms);

    HorizontalLayout searchForm = new HorizontalLayout(fieldsLayout, buttonsLayout);
    searchForm.setComponentAlignment(buttonsLayout, Alignment.MIDDLE_CENTER);

    VerticalLayout formContents = new VerticalLayout();
    formContents.setSizeFull();
    formContents.addComponent(searchForm);
    formContents.setComponentAlignment(searchForm, Alignment.MIDDLE_CENTER);
    formContents.addComponent(grid);

    formContents.setExpandRatio(searchForm, 0);
    formContents.setExpandRatio(grid, 5);

    panel.setContent(formContents);
    
    updateGrid();
    grid.getSelectionModel().setUserSelectionAllowed(false);

    return panel;
  }



private void  sendCommandAlarms() {
    String mci;
    if (meter.getSelectedItem().isPresent()) {
      mci = meter.getSelectedItem().get().getSerialNumber();
  	  CommandUndervoltagesRest command = new CommandUndervoltagesRest();
  	  command.sendCommandUndervoltages(mci);
    }else {
    	Notification.show(getMessage("SelectModule"), Type.WARNING_MESSAGE);
    }


}



public void updateGrid() {
    String serialNumber;
    if (meter.getSelectedItem().isPresent()) {
      serialNumber = meter.getSelectedItem().get().getSerialNumber();
      userInfo.setSelectedMeter(meter.getSelectedItem().get());
    } else {
      serialNumber = "";
      userInfo.setSelectedMeter(null);
    }
    ZonedDateTime start = startDate.getValue().atZone(userInfo.getTimezone());
    ZonedDateTime end = endDate.getValue().atZone(userInfo.getTimezone());
    logger.info("[{}] Buscando leituras do medidor [{}] de {} ate {}", userInfo.getUser(), serialNumber, start, end);
    lastReadings = alarmDAO.getAlarms(serialNumber, start, end);
    logger.info("[{}] {} leituras encontradas", userInfo.getUser(), lastReadings.size());

    grid.setSizeFull();
    grid.removeAllColumns();
    grid.setItems(lastReadings);
    
    grid.addColumn(reading -> reading.getMeter())
        .setExpandRatio(1)
        .setId("Module")
        .setCaption(getMessage("Module"));
    
    grid.addColumn(reading -> reading.getDatetime().toLocalDateTime())
        .setExpandRatio(1)
        .setRenderer(new LocalDateTimeRenderer("dd/MM/yyyy HH:mm"))
        .setId("Datetime")
        .setCaption(getMessage("Datetime"));

    grid.addColumn(reading -> reading.getStatusPhaseA())
        .setExpandRatio(1)
        .setId("StatusPhaseA")
        .setCaption(getMessageReading("StatusPhaseA", null, null));
    
    grid.addColumn(reading -> reading.getStatusPhaseB())
        .setExpandRatio(1)
        .setId("StatusPhaseB")
        .setCaption(getMessageReading("StatusPhaseB", null, null));
    grid.addColumn(reading -> reading.getStatusPhaseC())
        .setExpandRatio(1)
        .setId("StatusPhaseC")
        .setCaption(getMessageReading("StatusPhaseC", null, null));
    
}
  
  private void exportGridExcel() {
    if (lastReadings.isEmpty())
      return;
    
    logger.info("[{}] Exportando {} leituras", userInfo.getUser(), lastReadings.size());
    Workbook wb = new HSSFWorkbook();
    CreationHelper createHelper = wb.getCreationHelper();
    CellStyle dateStyle = wb.createCellStyle();
    dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));
    
    Sheet sheet;
    if (userInfo.isDisplayPowerValues()) {
        sheet = createSheet(wb, getMessage("Title"),
        	getMessageReading("Module", null, null),	
        	getMessageReading("StatusPhaseA", null, null),	
        	getMessageReading("StatusPhaseB", null, null),
            getMessageReading("StatusPhaseC", null, null));
      } else {
        sheet = createSheet(wb, getMessage("AlarmsHistory"),
        		getMessageReading("Module", null, null),
            	getMessageReading("StatusPhaseA", null, null),	
            	getMessageReading("StatusPhaseB", null, null),
                getMessageReading("StatusPhaseC", null, null));
      }
    
    int row = 1;
    for (Alarm r : lastReadings) {
      ZonedDateTime dt = r.getDatetime();

      
      if (userInfo.isDisplayPowerValues()) {
          addRow(sheet, dateStyle, row++, dt,
        	   r.getMeter(),
               r.getStatusPhaseA(),
               r.getStatusPhaseB(),
               r.getStatusPhaseC());

        } else {
          addRow(sheet, dateStyle, row++, dt,
        	  r.getMeter(),
              r.getStatusPhaseA(),
              r.getStatusPhaseB(),
              r.getStatusPhaseC());

        }
      }
    
    String meter = "All";
    if (userInfo.getSelectedMeter() != null) {
      meter = userInfo.getSelectedMeter().getSerialNumber();
    }

    File tempFile = null;
    FileOutputStream fileOut = null;
    try {
      tempFile = File.createTempFile("tmp", ".xls");
      fileOut = new FileOutputStream(tempFile);
      wb.write(fileOut);
      fileOut.flush();
      sendFileToUser(tempFile, "application/vnd.ms-excel",
          "AlarmsHistory" + "-" + meter + "-"
              + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"))
              + ".xls");
    } catch (final IOException e) {
      LoggerFactory.getLogger(getClass()).error("Converting to XLS failed", e);
    } finally {
      tempFile.deleteOnExit();
      try {
        fileOut.close();
      } catch (final IOException e) {
        LoggerFactory.getLogger(getClass()).warn("Leaking resource", e);
      }
    }
  }
  




private Sheet createSheet(Workbook wb, String title, String... colluns) {
    Sheet sheet = wb.createSheet(WorkbookUtil.createSafeSheetName(title));
    Row h = sheet.createRow(0);
    h.createCell(0).setCellValue(getMessage("Datetime"));
    sheet.setColumnWidth(0, 15 * 256);
    for (int cell = 0; cell < colluns.length; cell++) {
      h.createCell(cell+1).setCellValue(colluns[cell]);
      sheet.setColumnWidth(cell+1, 20 * 256);
    }
    return sheet;
  }
  

  private void addRow(Sheet sheet, CellStyle dateStyle, int row, ZonedDateTime datetime, String... values) {
	    Row h = sheet.createRow(row);
	    Cell cellDate = h.createCell(0);
	    cellDate.setCellValue(GregorianCalendar.from(datetime));
	    cellDate.setCellStyle(dateStyle);  
	    for (int i = 0; i < values.length; i++) {
	      String cell = values[i] != null ? (values[i]) : "";
	      h.createCell(i+1).setCellValue(cell);
	    }
	  }
  
}