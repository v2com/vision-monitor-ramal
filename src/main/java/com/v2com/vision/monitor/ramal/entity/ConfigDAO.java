package com.v2com.vision.monitor.ramal.entity;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.io.BufferedReader;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.util.Nonbinding;
import javax.inject.Named;
import javax.inject.Qualifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigDAO {
  
  private Logger logger = LoggerFactory.getLogger(getClass());

  @Property
  @Produces
  public String produceString(final InjectionPoint ip) {
    return getProperties().getProperty(getKey(ip));
  }

  @Property
  @Produces
  public int produceInt(final InjectionPoint ip) {
    return Integer.valueOf(getProperties().getProperty(getKey(ip)));
  }

  @Property
  @Produces
  public boolean produceBoolean(final InjectionPoint ip) {
    return Boolean.valueOf(getProperties().getProperty(getKey(ip)));
  }

  private String getKey(final InjectionPoint ip) {
    if (ip.getAnnotated().isAnnotationPresent(Property.class) && 
        !ip.getAnnotated().getAnnotation(Property.class).value().isEmpty()) {
      return ip.getAnnotated().getAnnotation(Property.class).value();
    } else {
      return ip.getMember().getName();
    }
  }

  @Produces
  @Named("configuracoes")
  public Properties getProperties() {
    Properties properties = new Properties();
    Path path = Paths.get(System.getProperty("jboss.server.config.dir"), "vision.properties");
    try (BufferedReader reader = Files.newBufferedReader(path)) {
      properties.load(reader);
    } catch (Exception e) {
      logger.error("Carregando arquivo de configuracao", e);
      throw new IllegalStateException(e);
    }
    return properties;
  }
  
  @Qualifier
  @Retention(RUNTIME)
  @Target({TYPE, METHOD, FIELD, PARAMETER})
  public @interface Property {

    @Nonbinding String value() default "";
    
    @Nonbinding boolean required() default true;
  }
  
}
