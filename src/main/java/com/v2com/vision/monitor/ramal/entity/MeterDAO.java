package com.v2com.vision.monitor.ramal.entity;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.entity.registry.Meter;

public class MeterDAO extends LocalDAO<Meter> {

  private Logger logger = LoggerFactory.getLogger(getClass());
  
  @Override
  protected Path getPath() {
    if (System.getProperty("jboss.server.config.dir") != null) {
      return Paths.get(System.getProperty("jboss.server.config.dir"), "meters.json");
    } else {
      return Paths.get("meters.json");
    }
  }
  
  public Meter findBySerialNumber(String serialNumber) {
    for (Meter meter : loadAll()) {
      if (meter.getSerialNumber().equals(serialNumber))
        return meter;
    }
    return null;
  }
  
  @Produces
  @Named("meters")
  public List<Meter> loadAll() {
    logger.info("Carregando meters...");
    return super.loadAll();
  }

}
