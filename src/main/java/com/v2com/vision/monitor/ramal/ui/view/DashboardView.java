package com.v2com.vision.monitor.ramal.ui.view;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.entity.ReadingDAO;
import com.v2com.vision.monitor.ramal.entity.ConfigDAO.Property;
import com.v2com.vision.monitor.ramal.entity.registry.Meter;
import com.v2com.vision.monitor.ramal.entity.registry.Reading;
import com.v2com.vision.monitor.ramal.ui.app.BaseView;
import com.v2com.vision.monitor.ramal.ui.navegation.NavigationEvent;
import com.v2com.vision.monitor.ramal.ui.user.UserInfo;
import com.v2com.vision.monitor.ramal.ui.util.MapBound;
import com.vaadin.cdi.CDIView;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.LocalDateTimeRenderer;
import com.vaadin.ui.themes.ValoTheme;

@CDIView(DashboardView.VIEWNAME)
public class DashboardView extends BaseView {

  private static final long serialVersionUID = 1L;

  public static final String VIEWNAME = "DashboardView";
  
  @Inject
  @Named("meters")
  List<Meter> meters;
  
  @Inject
  private UserInfo userInfo;
  
  @Inject
  private ReadingDAO readingDAO;
  
  @Inject
  private javax.enterprise.event.Event<NavigationEvent> navigationEvent;
  
  @Inject
  @Property("maps.apikey")
  private String apiKey;
  
  private GoogleMap googleMap;
  private Grid<Meter> grid = new Grid<>(Meter.class);
  private Logger logger = LoggerFactory.getLogger(getClass());
  private DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
  
  @Override
  protected Component createViewComponent() {
    Panel panel = new Panel(getMessage("Title"));
    panel.setSizeFull();
    
    VerticalLayout searchForm = new VerticalLayout(grid);
    searchForm.setSizeFull();
    grid.getSelectionModel().setUserSelectionAllowed(false);
    
//  googleMap = new GoogleMap(apiKey, null, null);
//  googleMap.setSizeFull();
//  googleMap.setZoom(12);
//  googleMap.setCenter(new LatLon(-22.239457, -48.049561));
//  googleMap.setMinZoom(4);
    
//  Panel mapsPanel = new Panel();
//  mapsPanel.setContent(googleMap);
//  mapsPanel.setSizeFull();

    HorizontalLayout formContents = new HorizontalLayout();
    formContents.setSizeFull();
    formContents.addComponent(searchForm);
//    formContents.addComponent(mapsPanel);
    panel.setContent(formContents);
    
    updateEvents();
    return panel;
  }

  public void updateEvents() {
    grid.setSizeFull();
    grid.removeAllColumns();
    grid.setItems(meters);
    
    grid.addComponentColumn(meter -> getMeterLink(meter)).setExpandRatio(1)
        .setCaption(getMessage("Module")).setId("Module");
    
    grid.addColumn(meter -> meter.getNome()).setExpandRatio(1)
        .setCaption(getMessage("Description")).setId("Description");
    
    grid.addColumn(meter -> lastReading(meter)).setExpandRatio(1)
        .setCaption(getMessage("Datetime")).setId("Datetime")
        .setRenderer(new LocalDateTimeRenderer(df));
    
//  MapBound mapBound = new MapBound();
//  List<LatLon> coords = new ArrayList<LatLon>();
//  meters.forEach(meter -> {
//    if (meter.getPosition() != null) {
//      LatLon coord = new LatLon(meter.getPosition().getX(), meter.getPosition().getY());
//      coords.add(coord);
//      mapBound.extendBounds(coord);
//    }
//  });
   // updateMarkers(true);
    
//  if (mapBound.getBoundsne() != null && mapBound.getBoundssw() != null) {
//  googleMap.setCenterBoundLimits(mapBound.getBoundsne(), mapBound.getBoundssw());
//}
}
  
  private LocalDateTime lastReading(Meter meter) {
    Reading reading = readingDAO.getLastReading(meter.getSerialNumber(), ZonedDateTime.now());
    return reading != null ? reading.getDatetime().toLocalDateTime() : null;
  }
  
  private Button getMeterLink(Meter meter) {
    Button button = new Button(meter.getSerialNumber());
    button.addStyleName(ValoTheme.BUTTON_LINK);
    button.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    button.addClickListener(click -> {
      logger.info("[{}] Medidor {} selecionado", userInfo.getUser(), meter.getSerialNumber());
      userInfo.setSelectedMeter(meter);
      navigationEvent.fire(new NavigationEvent(ChartsView.VIEWNAME));
    });
    return button;
  }

//private void updateMarkers(boolean animation) {
//googleMap.clearMarkers();
//meters.forEach(meter -> {
//  logger.debug("adicionando medidor {}", meter);
//  LatLon coord = new LatLon(meter.getPosition().getX(), meter.getPosition().getY());
//  GoogleMapMarker marker = new GoogleMapMarker(getMeterCaption(meter), coord, false, getMessage("YellowMarker"));
//  marker.setAnimationEnabled(animation);
//  googleMap.addMarker(marker);
//});
//}
  
  private String getMeterCaption(Meter meter) {
    LocalDateTime datetime = lastReading(meter);
    String strDatetime = datetime != null ? datetime.format(df) : "N/A";
    
    StringBuilder builder = new StringBuilder();
    builder.append(getMessage("Module")).append(": ").append(meter.getSerialNumber()).append("\r\n")
           .append(getMessage("Description")).append(": ").append(meter.getNome()).append("\r\n")
           .append(getMessage("Datetime")).append(": ").append(strDatetime).append("\r\n");
    return builder.toString();
  }
  
}