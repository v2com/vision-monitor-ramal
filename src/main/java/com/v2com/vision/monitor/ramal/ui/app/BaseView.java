package com.v2com.vision.monitor.ramal.ui.app;

import java.io.File;
import java.io.FileInputStream;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.ui.navegation.NavigationEvent;
import com.v2com.vision.monitor.ramal.ui.util.Messages;
import com.v2com.vision.monitor.ramal.ui.view.AlarmsView;
import com.v2com.vision.monitor.ramal.ui.view.ChartsView;
import com.v2com.vision.monitor.ramal.ui.view.DashboardView;
import com.v2com.vision.monitor.ramal.ui.view.ReadingsView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.ClassResource;
import com.vaadin.server.DownloadStream;
import com.vaadin.server.Page;
import com.vaadin.server.ResourceReference;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

public abstract class BaseView extends AbsoluteLayout implements View {
  private static final long serialVersionUID = 1L;

  private MenuBar menuBar;
  private MenuItem logout;
  private MenuBar.Command goTo;
  
  protected MenuItem dashboard;
  protected MenuItem charts;
  protected MenuItem readings;
  protected MenuItem alarms;

  @Inject
  private Messages messages;
  
  @Inject
  private javax.enterprise.event.Event<NavigationEvent> navigationEvent;
  
  private Component viewComponent;
  
  /**
   * Provides the navigation bar at the top of the page for the views.
   */

  @PostConstruct
  public void init() {
    goTo = new MenuBar.Command() {

      private static final long serialVersionUID = 1L;

      @Override
      public void menuSelected(MenuItem selectedItem) {
        selectedItem.setChecked(true);
        if (selectedItem.equals(logout)) {
          logout();
        }  else if (selectedItem.equals(dashboard)) {
          navigationEvent.fire(new NavigationEvent(DashboardView.VIEWNAME));
        } else if (selectedItem.equals(charts)) {
          navigationEvent.fire(new NavigationEvent(ChartsView.VIEWNAME));
        } else if (selectedItem.equals(readings)) {
          navigationEvent.fire(new NavigationEvent(ReadingsView.VIEWNAME));
        }else if (selectedItem.equals(alarms)) {
            navigationEvent.fire(new NavigationEvent(AlarmsView.VIEWNAME));
          }
      }
    };

    menuBar = new MenuBar();
    dashboard = menuBar.addItem(getMessage("DashboardMenuItem"), VaadinIcons.MAP_MARKER, goTo);
    charts = menuBar.addItem(getMessage("ChartsMenuItem"), VaadinIcons.CHART, goTo);
    readings = menuBar.addItem(getMessage("ReadingsMenuItem"), VaadinIcons.RECORDS, goTo);
    alarms = menuBar.addItem(getMessage("AlarmsMenuItem"), VaadinIcons.RECORDS, goTo);
    logout = menuBar.addItem(getMessage("LogoutMenuItem"), VaadinIcons.SIGN_OUT_ALT, goTo);
    

    this.addComponent(menuBar, "top:10px; left:10px");

    this.addComponent(new Image(null, new ClassResource(getMessage("ClientLogoPath"))), getMessage("ClientLogoCSS"));

    viewComponent = createViewComponent();

    this.addComponent(viewComponent, "top:90px; left:10px");
  }
  
  private void logout() {
    try {
      UI.getCurrent().getSession().close();
      VaadinService.getCurrentRequest().getWrappedSession().invalidate();
      Page.getCurrent().setLocation("");
    } catch (Exception e) {
      Notification.show(getMessage("LogoutFailed"), Type.WARNING_MESSAGE);
    }
  }
  
  protected abstract Component createViewComponent();
  
  protected String getMessage(Object key) {
    if (key == null) {
      return "-";
    } else if (key instanceof Enum) {
      return messages.getMessage(this.getClass(), key.getClass().getSimpleName() + "." + ((Enum<?>) key).name());
    } else if (key instanceof String) {
      return messages.getMessage(this.getClass(), (String) key);
    } else {
      return messages.getMessage(this.getClass(), key.toString());
    }

  }

  protected String getMessage(String key) {
    return messages.getMessage(this.getClass(), key);
  }
  
  protected String getMessage(String key, Object... params) {
    return messages.getMessage(this.getClass(), key, params);
  }

  protected String getMessageByKey(String key) {
    return messages.getMessage(key);
  }
  
//  protected String getMessageConcat(String key1, String key2, String sep) {
//    String msg1 = messages.getMessage(this.getClass(), key1);
//    String msg2 = messages.getMessage(this.getClass(), key2);
//    return msg1 + sep + msg2;
//  }
  
  protected String getMessageReading(String value, String phase, String unit) {
    StringBuilder builder = new StringBuilder();
    builder.append(messages.getMessage(this.getClass(), value));
    if (phase != null) {
      builder.append(" - ").append(messages.getMessage(this.getClass(), phase));
    }
    if (unit != null) {
      builder.append(" ").append(messages.getMessage(this.getClass(), unit));
    }
    return builder.toString();
  }
  
  protected boolean sendFileToUser(File fileToExport, String mimeType, String exportFileName) {
    try {
      StreamResource.StreamSource source = () -> {
        try {
          return new FileInputStream(fileToExport);
        } catch (Exception e) {
          LoggerFactory.getLogger(getClass()).debug("Error creating file", e);
        }
        return null;
      };

      StreamResource resource = new StreamResource(source, "EventHistory.pdf") {
        private static final long serialVersionUID = 5697228378233900981L;
        DownloadStream downloadStream;

        @Override
        public DownloadStream getStream() {
          if (downloadStream == null) {
            downloadStream = super.getStream();
          }
          return downloadStream;
        }
      };
      resource.getStream().setParameter("Content-Disposition",
          "attachment;filename=\"" + exportFileName + "\"");
      resource.getStream().setParameter("Content-Type", mimeType);
      resource.getStream().setCacheTime(0);
      ResourceReference ref = new ResourceReference(resource, this, "download");
      this.setResource("download", resource); // now it's available for download
      Page.getCurrent().open(ref.getURL(), null);
      return true;
    } catch (Exception e) {
      LoggerFactory.getLogger(getClass()).error("Sending file to user failed", e);
      return false;
    }
  }
  
}
