/**
 * 
 */

package com.v2com.vision.monitor.ramal.ui.navegation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.inject.Qualifier;

@Retention(RUNTIME)
@Target(FIELD)
@Qualifier
/**
 * @author llima
 *
 */
public @interface MainView {

  public static enum Type {
    LOGIN, MAIN;
  }
  
  /**
   * Type of this MainView: Login or Main.
   * @return the type of this view.
   */
  Type value();
}
