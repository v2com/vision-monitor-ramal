package com.v2com.vision.monitor.ramal.ui.user;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Locale;

import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import com.v2com.vision.monitor.ramal.entity.registry.User;
import com.v2com.vision.monitor.ramal.entity.registry.UserDAO;
import com.v2com.vision.monitor.ramal.ui.util.Messages;
import com.vaadin.cdi.access.AccessControl;

@Alternative
public class LocalAccessControl extends AccessControl implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Inject
  private UserDAO userDAO;
  
  @Inject
  private UserInfo userInfo;
  
  @Inject
  private Messages messages;

  @Override
  public boolean isUserSignedIn() {
    return userInfo.getUser() != null;
  }

  @Override
  public boolean isUserInRole(String role) {
    if (isUserSignedIn()) {
      return userInfo.getGroups().contains(role);
    }
    return false;
  }

  @Override
  public String getPrincipalName() {
    if (isUserSignedIn()) {
      return userInfo.getUser();
    }
    return null;
  }

  public boolean doLogin(String username, String password) {
    User user = userDAO.findByLogin(username);
    if (user == null || !password.equals(user.getPassword()))
      return false;
    
    userInfo.setUser(user.getLogin());
    userInfo.setDisplayPowerValues(user.isDisplayPowerValues());
    if (user.getLocale() != null) {
      userInfo.setLocale(Locale.forLanguageTag(user.getLocale()));
      messages.loadResourceBundle();
    }
    
    if (user.getTimezone() != null) {
      userInfo.setTimezone(ZoneId.of(user.getTimezone()));
    }
    
    return true;
  }

}
