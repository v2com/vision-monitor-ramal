package com.v2com.vision.monitor.ramal.ui.util;

import org.jsoup.nodes.Element;

import com.vaadin.server.Resource;
import com.vaadin.ui.CheckBoxGroup;
import com.vaadin.ui.declarative.DesignAttributeHandler;
import com.vaadin.ui.declarative.DesignContext;

public class CheckBoxGroupIcon<T> extends CheckBoxGroup<T> {
  
  private static final long serialVersionUID = 1L;
  
  public CheckBoxGroupIcon(String caption) {
    super(caption);
  }

  @Override
  protected Element writeItem(Element design, T item, DesignContext context) {
    Element element =  super.writeItem(design, item, context);
    Resource icon = getItemIconGenerator().apply(item);
    if (icon != null) {
        DesignAttributeHandler.writeAttribute("icon", element.attributes(),
                icon, null, Resource.class, context);
    }
    return element;
  }

}
