package com.v2com.vision.monitor.ramal.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures a JAX-RS endpoint. Delete this class, if you are not exposing
 * JAX-RS resources in your application.
 * 
 * @author airhacks.com
 */
@ApplicationPath("resources")
public class JaxrsConfiguration extends Application {

}
