package com.v2com.vision.monitor.ramal.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LocalDAO<T> {

  private Logger logger = LoggerFactory.getLogger(getClass());

  protected abstract Path getPath();

  private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

  private Class<T> clazz;
  private Class<T[]> clazzArray;

  @SuppressWarnings("unchecked")
  public LocalDAO() {
    clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    clazzArray = (Class<T[]>) Array.newInstance(clazz, 0).getClass();
  }

  public List<T> loadAll() {
    try (BufferedReader reader = Files.newBufferedReader(getPath())) {
      return Arrays.asList(gson.fromJson(reader, clazzArray));
    } catch (Exception e) {
      logger.error("Carregando dados", e);
      throw new IllegalStateException(e);
    }
  }
}
