package com.v2com.vision.monitor.ramal.ui.navegation;

import java.io.Serializable;
import javax.enterprise.event.Observes;

public interface NavigationService extends Serializable {

  public void onNavigationEvent(@Observes NavigationEvent event);
}
