package com.v2com.vision.monitor.ramal.rest;

public class enviadorWits {
	
	private String mci;
	private String command;

	
	public String getMci() {
		return mci;
	}
	
	public void setMci(String mci) {
		this.mci = mci;
	}
	
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommandUndervoltagesRest [mci=").append(mci)
				.append(", command=").append(command)
				.append("]");
		return builder.toString();
	}



}
