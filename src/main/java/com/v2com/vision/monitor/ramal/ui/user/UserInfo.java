package com.v2com.vision.monitor.ramal.ui.user;

import com.v2com.vision.monitor.ramal.entity.registry.Meter;
import com.vaadin.cdi.UIScoped;
import java.io.Serializable;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@UIScoped
public class UserInfo implements Serializable {
  private static final long serialVersionUID = -5572211069055847954L;
  private List<String> groups;
  private String user;
  
 private Locale locale = new Locale("pt", "BR");
  
 private ZoneId timezone = ZoneId.of("America/Sao_Paulo");
  
  // private Locale locale = new Locale("en", "US");
  
//  private ZoneId timezone = ZoneId.of("America/Chicago");
  
  private Meter selectedMeter;
 
  private boolean displayPowerValues;

  public UserInfo() {} // this is for CDI

  public void setUser(String user) {
    this.user = user;
  }

  public String getUser() {
    return user;
  }
  
  public Locale getLocale() {
    return locale;
  }
  
  public void setLocale(Locale locale) {
    this.locale = locale;
  }
  
  public ZoneId getTimezone() {
    return timezone;
  }
  
  public void setTimezone(ZoneId timezone) {
    this.timezone = timezone;
  }
  
  public Meter getSelectedMeter() {
    return selectedMeter;
  }
  
  public void setSelectedMeter(Meter selectedMeter) {
    this.selectedMeter = selectedMeter;
  }
  
  public boolean isDisplayPowerValues() {
    return displayPowerValues;
  }
  
  public void setDisplayPowerValues(boolean displayPowerValues) {
    this.displayPowerValues = displayPowerValues;
  }
  
  public List<String> getGroups() {
    return groups;
  }

  public void setGroups(List<String> groups) {
    this.groups = Collections.unmodifiableList(new ArrayList<>(groups));
  }

}