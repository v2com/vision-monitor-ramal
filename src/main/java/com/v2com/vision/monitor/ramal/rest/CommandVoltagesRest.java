package com.v2com.vision.monitor.ramal.rest;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;


public class CommandVoltagesRest {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public void sendCommandVoltages (String mci ) {
		String url = "http://172.31.7.161:8080/Wits35/rest/commands/reading/voltages/"+ mci + "/VOLTAGES/";
		Client client = ClientBuilder.newClient();
		enviadorWits commandVoltages = new enviadorWits();
		commandVoltages.setMci(mci);
		commandVoltages.setCommand("VOLTAGES");
		try {
			WebTarget target = client.target(url);
			Response response = target.request().post(Entity.entity(commandVoltages, MediaType.APPLICATION_JSON));
			if (response.getStatus() == 200) {
				logger.info("Comando enviado com sucesso! " + response.readEntity(String.class));
				Notification.show("Comando enviado!", Type.WARNING_MESSAGE);
			} else {
				logger.warn("NÃ£o conseguiu resposta remota: " + url + " " + response.getStatus());
				Notification.show("Não conseguiu resposta remota!", Type.WARNING_MESSAGE);
				String resp = response.readEntity(String.class);
				logger.warn("Resp: " + resp);
			}
		} catch (Exception e) {
			logger.error("Falha ao enviar comando " + url, e);
			Notification.show("Falha ao enviar comandod!", Type.ERROR_MESSAGE);
		} finally {
			client.close();
		}
	}

	
	}
