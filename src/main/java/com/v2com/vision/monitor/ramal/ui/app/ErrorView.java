package com.v2com.vision.monitor.ramal.ui.app;

import javax.inject.Inject;

import com.v2com.vision.monitor.ramal.ui.login.LoginView;
import com.v2com.vision.monitor.ramal.ui.navegation.NavigationEvent;
import com.v2com.vision.monitor.ramal.ui.user.LocalAccessControl;
import com.v2com.vision.monitor.ramal.ui.view.DashboardView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ErrorView extends CustomComponent implements View {

  private static final long serialVersionUID = -5969825355454215860L;

  @Inject
  private LocalAccessControl accessControl;

  @Inject
  private javax.enterprise.event.Event<NavigationEvent> navigationEvent;

  @Override
  public void enter(ViewChangeEvent event) {
    VerticalLayout layout = new VerticalLayout();
    layout.setSizeFull();

    layout.addComponent(
        new Label("Unfortunately, the page you've requested does not exists."));
    if (accessControl.isUserSignedIn()) {
      layout.addComponent(createMainButton());
    } else {
      layout.addComponent(createLoginButton());
    }
    setCompositionRoot(layout);
  }

  @SuppressWarnings("serial")
  private Button createLoginButton() {
    Button button = new Button("To login page");
    button.addClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        navigationEvent.fire(new NavigationEvent(LoginView.VIEWNAME));
      }
    });
    return button;
  }

  @SuppressWarnings("serial")
  private Button createMainButton() {
    Button button = new Button("Back to the Query page");
    button.addClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        navigationEvent.fire(new NavigationEvent(DashboardView.VIEWNAME));
      }
    });
    return button;
  }
}
