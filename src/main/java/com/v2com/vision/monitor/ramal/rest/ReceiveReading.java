package com.v2com.vision.monitor.ramal.rest;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.entity.ConfigDAO.Property;
import com.v2com.vision.monitor.ramal.entity.MeterDAO;
import com.v2com.vision.monitor.ramal.entity.ReadingDAO;
import com.v2com.vision.monitor.ramal.entity.registry.Meter;
import com.v2com.vision.monitor.ramal.entity.registry.Reading;

@Path("/readings")
@Consumes(MediaType.APPLICATION_JSON)
public class ReceiveReading {

  @Inject
  private ReadingDAO readingDAO;
  
  @Inject
  private MeterDAO meterDAO;
  
  @Inject
  @Property("receiver.leitura.fuso")
  private String fuso;
  
  private Logger logger = LoggerFactory.getLogger(getClass());
  private DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss");
  
  @POST
  @Path("/salvar")
  @Transactional
  @Consumes(MediaType.APPLICATION_JSON)
  public Response salvarLeitura(JsonObject params) {
    
    Meter module = getMeter(params.getString("mci"));
    if (module == null) {
      logger.info("Módulo com serial number {} nao encontrado", params.getString("mci"));
      return Response.status(Status.NOT_FOUND).type("text/plain").entity("Modulo nao encontrado").build();
    }

    logger.info("Módulo encontrado {}", module);
    Reading reading = new Reading();
    
    ZoneId zoneId = ZoneId.of(ZoneOffset.UTC.getId());
    try {
      zoneId = ZoneId.of(fuso);
    } catch (Exception e) {
      logger.error("Erro create zoneId from {}", fuso);
    }
    
    logger.debug("Config date time with zoneId {} => {}", zoneId, ZoneId.systemDefault());
    LocalDateTime dateTime = LocalDateTime.parse(params.getString("dateTime"), df);
    ZonedDateTime zonedDateTime = dateTime.atZone(zoneId);
    zonedDateTime = zonedDateTime.plusSeconds(30);
    zonedDateTime = zonedDateTime.withSecond(0);
    ZonedDateTime finalDateTime = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault());
    logger.debug("Parsed datetime {} => {}", dateTime, finalDateTime);
    reading.setDatetime(finalDateTime);
    
    reading.setMeter(module.getSerialNumber());
    reading.setVoltagePh1(safeFloat(params, "phaseA"));
    reading.setVoltagePh2(safeFloat(params, "phaseB"));
    reading.setVoltagePh3(safeFloat(params, "phaseC"));
    
    logger.info("Salvando leitura: {}", reading);
    readingDAO.save(reading);
    
    return Response.status(Status.OK).build();
  }

  private Meter getMeter(String serialNumber) {
    logger.info("Buscando módulo com serial number {}", serialNumber);
    try {
      return meterDAO.findBySerialNumber(serialNumber);
    } catch (Exception e) {
      logger.error("Falha ao buscar módulo {}", serialNumber, e);
      return null;
    }
  }
  
  private Float safeFloat(JsonObject params, String param) {
    try {
      return Float.parseFloat(params.getString(param));
    } catch (Exception e) {
      return null;
    }
  }
  
  
}