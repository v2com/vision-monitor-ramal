package com.v2com.vision.monitor.ramal.entity.registry;

public class Meter {
  
  private String nome;
  
  private String serialNumber;
  
  private XYZ position;
  
  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getSerialNumber() {
    return serialNumber;
  }
  
  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }
  
  public XYZ getPosition() {
    return position;
  }
  
  public void setPosition(XYZ position) {
    this.position = position;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nome == null) ? 0 : nome.hashCode());
    result = prime * result + ((position == null) ? 0 : position.hashCode());
    result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Meter other = (Meter) obj;
    if (nome == null) {
      if (other.nome != null)
        return false;
    } else if (!nome.equals(other.nome))
      return false;
    if (position == null) {
      if (other.position != null)
        return false;
    } else if (!position.equals(other.position))
      return false;
    if (serialNumber == null) {
      if (other.serialNumber != null)
        return false;
    } else if (!serialNumber.equals(other.serialNumber))
      return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Meter [nome=").append(nome)
        .append(", serialNumber=").append(serialNumber)
        .append(", position=").append(position)
        .append("]");
    return builder.toString();
  }

}
