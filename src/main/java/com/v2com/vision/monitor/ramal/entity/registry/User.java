package com.v2com.vision.monitor.ramal.entity.registry;

public class User {

  private String name;
  
  private String login;

  private String locale;
  
  private String password;
  
  private String timezone;
  
  private boolean displayPowerValues;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }
  
  public String getPassword() {
    return password;
  }
  
  public void setPassword(String password) {
    this.password = password;
  }
  
  public String getTimezone() {
    return timezone;
  }
  
  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }
  
  public boolean isDisplayPowerValues() {
    return displayPowerValues;
  }
  
  public void setDisplayPowerValues(boolean displayPowerValues) {
    this.displayPowerValues = displayPowerValues;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("User [name=").append(name)
        .append(", login=").append(login)
        .append(", locale=").append(locale)
        .append(", password=").append(password)
        .append(", timezone=").append(timezone)
        .append(", displayPowerValues=").append(displayPowerValues)
        .append("]");
    return builder.toString();
  }

}
