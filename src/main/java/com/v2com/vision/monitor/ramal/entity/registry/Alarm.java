package com.v2com.vision.monitor.ramal.entity.registry;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Alarm {
  
  @Id
  @GeneratedValue
  private long id;
  
  @Column(length = 100, nullable = false)
  private String meter;
  
  
  @Column(nullable = true)
  private String statusPhaseA;
  
  @Column(nullable = true)
  private String statusPhaseB;
  
  @Column(nullable = true)
  private String statusPhaseC;
  
  @Column(nullable = false)
  private ZonedDateTime datetime;
  
  public long getId() {
    return id;
  }

  protected void setId(long id) {
    this.id = id;
  }
  
  public String getMeter() {
    return meter;
  }
  
  public void setMeter(String meter) {
    this.meter = meter;
  }

   	  
  public ZonedDateTime getDatetime() {
    return datetime;
  }

  public void setDatetime(ZonedDateTime datetime) {
    this.datetime = datetime;
  }
  
  public String getStatusPhaseA() {
	 return statusPhaseA;
  }
	  
  public void setStatusPhaseA(String statusPhaseA) {
	 this.statusPhaseA = statusPhaseA;
  }
  
  public String getStatusPhaseB() {
	  return statusPhaseB;  
  }
  
  public void setStatusPhaseB( String statusPhaseB) {
	  this.statusPhaseB = statusPhaseB;
	  
  }
  
  public String getStatusPhaseC() {
	  return statusPhaseC;
  }

  public void setStatusPhaseC(String statusPhaseC) {
       this.statusPhaseC = statusPhaseC;
	  
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Alarm [id=").append(id)
        .append(", meter=").append(meter)
        .append(", statusPhaseA=").append(statusPhaseA)
        .append(", statusPhaseB=").append(statusPhaseB)
        .append(", statusPhaseC=").append(statusPhaseC)
        .append(", datetime=").append(datetime)
        .append("]");
    return builder.toString();
  }

}
