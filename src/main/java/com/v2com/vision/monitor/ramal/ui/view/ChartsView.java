package com.v2com.vision.monitor.ramal.ui.view;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.v2com.vision.monitor.ramal.entity.ReadingDAO;
import com.v2com.vision.monitor.ramal.entity.registry.Meter;
import com.v2com.vision.monitor.ramal.entity.registry.Reading;
import com.v2com.vision.monitor.ramal.ui.app.BaseView;
import com.v2com.vision.monitor.ramal.ui.user.UserInfo;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.AxisTitle;
import com.vaadin.addon.charts.model.AxisType;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import com.vaadin.addon.charts.model.HorizontalAlign;
import com.vaadin.addon.charts.model.LayoutDirection;
import com.vaadin.addon.charts.model.Legend;
import com.vaadin.addon.charts.model.PlotOptionsLine;
import com.vaadin.addon.charts.model.VerticalAlign;
import com.vaadin.addon.charts.model.ZoomType;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings({"deprecation","serial"})
@CDIView(ChartsView.VIEWNAME)
public class ChartsView extends BaseView {
  
  public static final String VIEWNAME = "ChartsView";

  private static final ZoneId UTC = ZoneId.of("UTC");
  
  private static final String DATETIME_PATTERN = "dd/MM/yyyy HH:mm";

  @Inject
  @Named("meters")
  List<Meter> meters;
  
  @Inject
  private UserInfo userInfo;
  
  @Inject
  private ReadingDAO readingDAO;

  private TabSheet tabCharts;
  private ComboBox<Meter> meter;
  private DateTimeField endDate;
  private DateTimeField startDate;
  private List<Reading> lastReadings;
  private DecimalFormat df = new DecimalFormat("#.##");
  private Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  protected Component createViewComponent() {
    Panel panel = new Panel(getMessage("Title"));
    panel.setSizeFull();
    
    meter = new ComboBox<Meter>(getMessage("Module"));
    meter.setEmptySelectionAllowed(false);
    meter.setItems(meters);
    meter.setTextInputAllowed(false);
    meter.setPopupWidth(null);
    meter.setWidth("280px");
    meter.setItemCaptionGenerator((ItemCaptionGenerator<Meter>) submeter -> {
      return submeter.getSerialNumber();
    });
    meter.focus();
    if (userInfo.getSelectedMeter() != null) {
      meter.setSelectedItem(userInfo.getSelectedMeter());
    } else if (!meters.isEmpty()) {
      meter.setSelectedItem(meters.get(0));
    }
    
    df.setMaximumFractionDigits(2);
    df.setMinimumFractionDigits(2);
    
    CssLayout alarmLayout = new CssLayout(meter) {
      @Override
      protected String getCss(Component c) {
        return "display: inline-block; margin-left: 8px; margin-right: 8px;";
      }
    };
    alarmLayout.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
    
    startDate = new DateTimeField(getMessage("StartDate"), LocalDateTime.now().minusDays(3));
    endDate = new DateTimeField(getMessage("EndDate"), LocalDateTime.now());

    startDate.setPlaceholder(getMessage("StartDate"));
    endDate.setPlaceholder(getMessage("EndDate"));

    startDate.setDateFormat(DATETIME_PATTERN);
    endDate.setDateFormat(DATETIME_PATTERN);

    CssLayout dateLayout = new CssLayout(startDate, endDate) {
      @Override
      protected String getCss(Component c) {
        return "display: inline-block; margin-left: 8px; margin-right: 8px;";
      }
    };
    dateLayout.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
    
    VerticalLayout fieldsLayout = new VerticalLayout(alarmLayout, dateLayout);
    fieldsLayout.setComponentAlignment(alarmLayout, Alignment.MIDDLE_CENTER);
    fieldsLayout.setComponentAlignment(dateLayout, Alignment.MIDDLE_CENTER);

    Button searchButton = new Button(FontAwesome.SEARCH);
    searchButton.addClickListener(clickListener -> updateList());

    Button excelButton = new Button(FontAwesome.FILE_EXCEL_O);
    excelButton.addClickListener(e -> exportGridExcel());

//    Button csvButton = new Button(FontAwesome.FILE_CODE_O);
//    csvButton.addClickListener(e -> export(CsvExport.class, ".csv"));
    
    HorizontalLayout buttonsLayout = new HorizontalLayout(searchButton, excelButton);

    HorizontalLayout searchForm = new HorizontalLayout(fieldsLayout, buttonsLayout);
    searchForm.setComponentAlignment(buttonsLayout, Alignment.MIDDLE_CENTER);
    
    tabCharts = new TabSheet();
    tabCharts.setHeight(100.0f, Unit.PERCENTAGE);
    tabCharts.addStyleName(ValoTheme.TABSHEET_FRAMED);
    tabCharts.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);

    VerticalLayout formContents = new VerticalLayout();
    formContents.setSizeFull();
    formContents.addComponent(searchForm);
    formContents.setComponentAlignment(searchForm, Alignment.MIDDLE_CENTER);
    formContents.addComponent(tabCharts);

    formContents.setExpandRatio(searchForm, 0);
    formContents.setExpandRatio(tabCharts, 5);
    
    panel.setContent(formContents);
    return panel;
  }

  @Override
  public void enter(ViewChangeEvent event) {
    super.enter(event);
    updateList();
  }
  
  private void updateList() {
    if (meter.getSelectedItem().isPresent()) {
      String serialNumber = meter.getSelectedItem().get().getSerialNumber();
      ZonedDateTime start = startDate.getValue().atZone(userInfo.getTimezone());
      ZonedDateTime end = endDate.getValue().atZone(userInfo.getTimezone());
      logger.info("[{}] Buscando leituras do modulos [{}] de {} ate {}", userInfo.getUser(), serialNumber, start, end);
      lastReadings = readingDAO.getReadings(serialNumber, start, end);
      logger.info("[{}] {} leituras encontradas", userInfo.getUser(), lastReadings.size());
      userInfo.setSelectedMeter(meter.getSelectedItem().get());
    } else {
      lastReadings = Collections.emptyList();
      userInfo.setSelectedMeter(null);
    }
    
    tabCharts.removeAllComponents();
    generateCharts();
  }

  private void generateCharts() {
      
   // Chart eChart = createChart(getMessage("Energy"), null);
   // Chart iChart = createChart(getMessage("Current"), getMessage("CurrentUnit"));
    Chart uChart = createChart(getMessage("Voltage"), getMessage("VoltageUnit"));
//  Chart pChart, qChart, sChart, fChart;
//  pChart = qChart = sChart = fChart = null;
//  if (userInfo.isDisplayPowerValues()) {
//        pChart = createChart(getMessage("ActivePower"), getMessage("ActivePowerUnit"));
//        qChart = createChart(getMessage("ReactivePower"), getMessage("ReactivePowerUnit"));
//        sChart = createChart(getMessage("ApparentPower"), getMessage("ApparentPowerUnit"));
//        fChart = createChart(getMessage("PowerFactor"), null);
//  }
    
    //DataSeries energAtiva = createDataSeries(eChart, getMessageReading("ActiveEnergy", null, "ActiveEnergyUnit"));
   // DataSeries energReativa = createDataSeries(eChart, getMessageReading("ReactiveEnergy", null, "ReactiveEnergyUnit"));
    
  //DataSeries iPh1 = createDataSeries(iChart, getMessageReading("CurrentMed", "Phase1", null));
  //DataSeries iPh2 = createDataSeries(iChart, getMessageReading("CurrentMed", "Phase2", null));
  //DataSeries iPh3 = createDataSeries(iChart, getMessageReading("CurrentMed", "Phase3", null));
  //DataSeries iMinPh1 = createDataSeries(iChart, getMessageReading("CurrentMin", "Phase1", null));
  //DataSeries iMinPh2 = createDataSeries(iChart, getMessageReading("CurrentMin", "Phase2", null));
  //DataSeries iMinPh3 = createDataSeries(iChart, getMessageReading("CurrentMin", "Phase3", null));
  //DataSeries iMaxPh1 = createDataSeries(iChart, getMessageReading("CurrentMax", "Phase1", null));
  //DataSeries iMaxPh2 = createDataSeries(iChart, getMessageReading("CurrentMax", "Phase2", null));
  //DataSeries iMaxPh3 = createDataSeries(iChart, getMessageReading("CurrentMax", "Phase3", null));
    
    DataSeries uPh1 = createDataSeries(uChart, getMessageReading("Voltage", "Phase1", null));
    DataSeries uPh2 = createDataSeries(uChart, getMessageReading("Voltage", "Phase2", null));
    DataSeries uPh3 = createDataSeries(uChart, getMessageReading("Voltage", "Phase3", null));
//  DataSeries uMinPh1 = createDataSeries(uChart, getMessageReading("VoltageMin", "Phase1", null));
//  DataSeries uMinPh2 = createDataSeries(uChart, getMessageReading("VoltageMin", "Phase2", null));
//  DataSeries uMinPh3 = createDataSeries(uChart, getMessageReading("VoltageMin", "Phase3", null));
//  DataSeries uMaxPh1 = createDataSeries(uChart, getMessageReading("VoltageMax", "Phase1", null));
//  DataSeries uMaxPh2 = createDataSeries(uChart, getMessageReading("VoltageMax", "Phase2", null));
//  DataSeries uMaxPh3 = createDataSeries(uChart, getMessageReading("VoltageMax", "Phase3", null));
    
//  DataSeries pPh1, pPh2, pPh3;
//  DataSeries qPh1, qPh2, qPh3;
//  DataSeries sPh1, sPh2, sPh3;
//  DataSeries fPh1, fPh2, fPh3;
    
//  pPh1 = pPh2 = pPh3 = null;
//  qPh1 = qPh2 = qPh3 = null;
//  sPh1 = sPh2 = sPh3 = null;
//  fPh1 = fPh2 = fPh3 = null;
    
//  if (userInfo.isDisplayPowerValues()) {
//  pPh1 = createDataSeries(pChart, getMessage("Phase1"));
//  pPh2 = createDataSeries(pChart, getMessage("Phase2"));
//  pPh3 = createDataSeries(pChart, getMessage("Phase3"));
//
//  qPh1 = createDataSeries(qChart, getMessage("Phase1"));
//  qPh2 = createDataSeries(qChart, getMessage("Phase2"));
//  qPh3 = createDataSeries(qChart, getMessage("Phase3"));
//
//  sPh1 = createDataSeries(sChart, getMessage("Phase1"));
//  sPh2 = createDataSeries(sChart, getMessage("Phase2"));
//  sPh3 = createDataSeries(sChart, getMessage("Phase3"));
//
//  fPh1 = createDataSeries(fChart, getMessage("Phase1"));
//  fPh2 = createDataSeries(fChart, getMessage("Phase2"));
//  fPh3 = createDataSeries(fChart, getMessage("Phase3"));
//}
    
    for (Reading reading : lastReadings) {
      Instant instant = reading.getDatetime().withZoneSameLocal(UTC).toInstant();
      addItem(uChart, uPh1, instant, reading.getVoltagePh1());
      addItem(uChart, uPh2, instant, reading.getVoltagePh2());
      addItem(uChart, uPh3, instant, reading.getVoltagePh3());

    }
  }
  
  private Chart createChart(String title, String unit) {
    Chart chart = new Chart();
    chart.setSizeFull();

    Configuration configuration = chart.getConfiguration();
    configuration.getChart().setType(ChartType.LINE);
    configuration.getxAxis().setType(AxisType.DATETIME);
    configuration.getLegend().setEnabled(true);
    configuration.setTitle((String)null);
    configuration.getChart().setZoomType(ZoomType.X);
    configuration.getyAxis().setTitle(new AxisTitle(title + (unit != null ? " " + unit : "")));
//    configuration.getyAxis().setMin(-2);
//    configuration.getyAxis().setMax(2);
    
    Legend legend = configuration.getLegend();
    legend.setLayout(LayoutDirection.VERTICAL);
    legend.setAlign(HorizontalAlign.RIGHT);
    legend.setVerticalAlign(VerticalAlign.TOP);
    legend.setX(-10d);
    legend.setY(100d);
    legend.setBorderWidth(0);
    
    tabCharts.addTab(chart, title);
    return chart;
}
  
  private DataSeries createDataSeries(Chart chart, String name) {
    DataSeries ds = new DataSeries(name);
    PlotOptionsLine  options = new PlotOptionsLine();
    options.getDataLabels().setEnabled(true);
    options.getDataLabels().setFormat("{point.y:,.2f}");
    options.getTooltip().setPointFormat("{series.name}: {point.y:,.2f}");
    options.getTooltip().setXDateFormat("%d/%m/%Y %H:%M");
    ds.setPlotOptions(options);
    chart.getConfiguration().addSeries(ds);
    return ds;
  }
  
  private void addItem(Chart chart, DataSeries ds, Instant inst, Float value) {
//    double min = chart.getConfiguration().getyAxis().getMin().doubleValue();
//    double max = chart.getConfiguration().getyAxis().getMax().doubleValue();
    ds.add(new DataSeriesItem(inst, value));
//    if (value != null) {
//      if (value > max) {
//        chart.getConfiguration().getyAxis().setMax(value);
//      } else if (value < min) {
//        chart.getConfiguration().getyAxis().setMin(value);
//      }
//    }
  }
  
  private void exportGridExcel() {
    if (lastReadings.isEmpty())
      return;
    
    logger.info("[{}] Exportando {} leituras", userInfo.getUser(), lastReadings.size());
    Workbook wb = new HSSFWorkbook();
    CreationHelper createHelper = wb.getCreationHelper();
    CellStyle dateStyle = wb.createCellStyle();
    dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));
    
         
    Sheet uSheet = createSheet(wb, getMessage("Voltage"),
        getMessageReading("Voltage", "Phase1", "VoltageUnit"),
        getMessageReading("Voltage", "Phase2", "VoltageUnit"),
        getMessageReading("Voltage", "Phase3", "VoltageUnit"));
//      getMessageReading("VoltageMed", "Phase2", "VoltageUnit"),
//      getMessageReading("VoltageMin", "Phase2", "VoltageUnit"),
//      getMessageReading("VoltageMax", "Phase2", "VoltageUnit"),
//      getMessageReading("VoltageMed", "Phase3", "VoltageUnit"),
//      getMessageReading("VoltageMin", "Phase3", "VoltageUnit"),
//      getMessageReading("VoltageMax", "Phase3", "VoltageUnit"));
    
    Sheet pSheet, qSheet, sSheet, fSheet;
    pSheet = qSheet = sSheet = fSheet = null;
//  if (userInfo.isDisplayPowerValues()) {
//  pSheet = createSheet(wb, getMessage("ActivePower"),
//      getMessageReading("ActivePower", "Phase1", "ActivePowerUnit"),
//      getMessageReading("ActivePower", "Phase2", "ActivePowerUnit"),
//      getMessageReading("ActivePower", "Phase3", "ActivePowerUnit"));
//
//  qSheet = createSheet(wb, getMessage("ReactivePower"),
//      getMessageReading("ReactivePower", "Phase1", "ReactivePowerUnit"),
//      getMessageReading("ReactivePower", "Phase2", "ReactivePowerUnit"),
//      getMessageReading("ReactivePower", "Phase3", "ReactivePowerUnit"));
//
//  sSheet = createSheet(wb, getMessage("ApparentPower"),
//      getMessageReading("ApparentPower", "Phase1", "ApparentPowerUnit"),
//      getMessageReading("ApparentPower", "Phase2", "ApparentPowerUnit"),
//      getMessageReading("ApparentPower", "Phase3", "ApparentPowerUnit"));
//
//  fSheet = createSheet(wb, getMessage("PowerFactor"),
//      getMessageReading("PowerFactor", "Phase1", null),
//      getMessageReading("PowerFactor", "Phase2", null),
//      getMessageReading("PowerFactor", "Phase3", null));
//}
    
    int row = 1;
    for (Reading r : lastReadings) {
      ZonedDateTime dt = r.getDatetime();
     // addRow(eSheet, dateStyle, row, dt, r.getActiveEnergy(), r.getReactiveEnergy());
      
     // addRow(iSheet, dateStyle, row, dt,
    		//r.getCurrentPh1(),
    		//r.getCurrentMinPh1(),
    		//r.getCurrentMaxPh1(),
    		//r.getCurrentPh2(),
    		//r.getCurrentMinPh2(),
    		//r.getCurrentMaxPh2(),
    		//r.getCurrentPh3(),
    		//r.getCurrentMinPh3(),
    		//r.getCurrentMaxPh3());
      
      addRow(uSheet, dateStyle, row, dt,
          r.getVoltagePh1(),
          r.getVoltagePh2(),
          r.getVoltagePh3());   
      row++;
    }
    
    String meter = "All";
    if (userInfo.getSelectedMeter() != null) {
      meter = userInfo.getSelectedMeter().getSerialNumber();
    }

    File tempFile = null;
    FileOutputStream fileOut = null;
    try {
      tempFile = File.createTempFile("tmp", ".xls");
      fileOut = new FileOutputStream(tempFile);
      wb.write(fileOut);
      fileOut.flush();
      sendFileToUser(tempFile, "application/vnd.ms-excel",
          "ChartsHistory-" + meter + "-"
              + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"))
              + ".xls");
    } catch (final IOException e) {
      LoggerFactory.getLogger(getClass()).error("Converting to XLS failed", e);
    } finally {
      tempFile.deleteOnExit();
      try {
        fileOut.close();
      } catch (final IOException e) {
        LoggerFactory.getLogger(getClass()).warn("Leaking resource", e);
      }
    }
  }
  
  private Sheet createSheet(Workbook wb, String title, String... colluns) {
    Sheet sheet = wb.createSheet(WorkbookUtil.createSafeSheetName(title));
    Row h = sheet.createRow(0);
    h.createCell(0).setCellValue(getMessage("Datetime"));
    sheet.setColumnWidth(0, 15 * 256);
    for (int cell = 0; cell < colluns.length; cell++) {
      h.createCell(cell+1).setCellValue(colluns[cell]);
      sheet.setColumnWidth(cell+1, 20 * 256);
    }
    return sheet;
  }
  
  private void addRow(Sheet sheet, CellStyle dateStyle, int row, ZonedDateTime datetime, Float... values) {
    Row h = sheet.createRow(row);
    Cell cellDate = h.createCell(0);
    cellDate.setCellValue(GregorianCalendar.from(datetime));
    cellDate.setCellStyle(dateStyle);        
    for (int i = 0; i < values.length; i++) {
      String cell = values[i] != null ? df.format(values[i]) : "";
      h.createCell(i+1).setCellValue(cell);
    }
  }
  
}
